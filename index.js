/**
  @copyright 2018-2019  Alfredo Mungo <alfredo.mungo@protonmail.ch>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to
  deal in the Software without restriction, including without limitation the
  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
  sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
  IN THE SOFTWARE.
*/
require('polyfill-object.fromentries');

const path = require('path');
const fs = require('fs');
const { slash } = require('slashf');

const DEFAULT_CONFIG_DIR = 'cfg';
const DEFAULT_TASK_DIR = 'tasks';

/**
  Resolve paths in a configuration object.

  The following rules apply:
  * Dollar substitution only applies to the first entry of the path
  * Behaviour for path names containing forward slashes is **undefined**
  * Regardless of the operating system, input paths must be all
    forward-slash-separated

  @param {Object} paths - Configuration object containing (only) the paths to
  resolve
  @param [opts] - Optional settings

  Options
  @param opts.baseDir - Base folder path for relative entries resolution
  @param [opts.iterationLimit = 1000] - Iteration limit to avoid circular
  dependencies
  @param [opts.normalize = false] - True to normalize all (output) slashes to
  forward slashes; if false, OS conventions will apply
  @param [opts.relative = false] - True to make all paths relative to `baseDir`

  @returns An object with the same shape as `paths` and the (resolvable) paths
  resolved

  @example
  paths = {
    myFolder1: '.',
    myFolder2: '/home',
    specialFile: '$myFolder2/special.txt'
  }

  resolvePaths(paths) == {
    myFolder1: '/my/current/working/directory/full/path',
    myFolder2: '/home',
    specialFile: '/home/special.txt'
  }
*/
function resolvePaths(paths, opts = {}) {
  const opts2 = {
    normalize: false,
    relative: false,
    iterationLimit: 1000,
    baseDir: process.cwd(),
    ...opts,
  };

  if (!Number.isInteger(opts2.iterationLimit)) throw new Error('"iterationLimit" must be an integer');
  if (typeof opts2.baseDir !== 'string') throw new Error('"baseDir" must be a string');

  // Fixes problem with paths when using junctions on NTFS
  opts2.baseDir = fs.realpathSync(opts2.baseDir);

  // Load the paths and resolve those that don't depend on any other
  // path in the config
  const paths2 = Object.fromEntries(
    Object.entries(paths).map(
      ([k, v]) => [
        k,
        v.startsWith('$')
          ? v
          : path.resolve(
            ...[
              ...[v.startsWith('/') ? '/' : opts2.baseDir],
              ...v.split('/'),
            ],
          ),
      ],
    ),
  );

  let iterationLimit; // Path substitution iteration limit to avoid infinite-loops

  // Substitute paths until all are processed
  for (
    { iterationLimit } = opts2;
    iterationLimit;
    iterationLimit -= 1
  ) {
    let subs = 0; // paths substituted during this iteration

    Object.keys(paths2).forEach((pName) => { // eslint-disable-line no-loop-func
      const p = paths2[pName];

      if (p.startsWith('$')) {
        const pathParts = p.split('/');
        const pathRoot = pathParts[0].substring(1);

        if (paths2[pathRoot]) {
          pathParts[0] = paths2[pathRoot];
          paths2[pName] = path.join.apply(undefined, pathParts);
          subs += 1;
        }
      }
    });

    if (!subs) {
      break;
    }
  }

  if (!iterationLimit) throw new Error('Configuration path substitution: iteration limit reached');

  return Object.fromEntries(
    Object.entries(paths2).map((pathSpec) => {
      let [k, v] = pathSpec; // eslint-disable-line prefer-const

      if (v.startsWith('$')) throw new Error(`Configuration path substitution: path ${k} not defined`);

      // Absolute to relative transformation
      if (opts2.relative) v = path.relative(opts2.baseDir, v);

      // Slash normalization
      if (opts2.normalize) v = slash(v);

      return [k, v];
    })
  );
}

/**
  Read configuration files from folder and return an object containing the
  exported configuration.

  @param {String} [cfgDir] - Configuration folder path
  @param {Object} [opts] - Optional settings

  Options
  @param [opts.exclude] - String or array of strings representing the module
    names to exclude
  @param [opts.transform] - Transformation function with prototype
    `function (modName, modContent) -> newModName` for the imported module names

  @returns An object with shape `{newModName -> modContent}`
*/
function readConfig(cfgDir = DEFAULT_CONFIG_DIR, opts = {}) {
  /* eslint-disable no-param-reassign, prefer-rest-params */
  if (arguments.length === 1 && typeof arguments[0] === 'object') {
    [opts] = arguments;
    cfgDir = DEFAULT_CONFIG_DIR;
  }

  cfgDir = path.resolve(cfgDir);
  /* eslint-enable no-param-reassign, prefer-rest-params */

  let { exclude } = opts;
  const { transform } = opts;

  if (!cfgDir || typeof cfgDir !== 'string') throw new Error('"cfgDir" must be a valid path');
  if (transform && typeof transform !== 'function') throw new Error('"transform" must be a function');
  if (exclude) {
    if (!Array.isArray(exclude)) exclude = [exclude]; // eslint-disable-line no-param-reassign
    if (exclude.length && typeof exclude[0] !== 'string') throw new Error('"exclude" must be an array of strings or a single string');
  }

  const files = fs.readdirSync(cfgDir)
    .map(fName => (fName.endsWith('.js') && fName.substring(0, fName.length - 3)) || fName)
    .filter(
      mName => (
        !(exclude || [])
          .includes(mName)
      )
    );
  const transformFunc = transform || (x => x);

  return Object.fromEntries(
    files.map(
      (modName) => {
        const modContent = require( // eslint-disable-line global-require, import/no-dynamic-require
          slash(path.join(cfgDir, modName))
        );

        return [
          transformFunc(modName, modContent),
          modContent
        ];
      }
    )
  );
}

/**
  Read task files from folder and return an object containing the
  exported tasks.

  @param {String} [taskDir] - Task folder path
  @param {Object} [opts] - Optional settings

  Options
  @param [opts.exclude] - String or array of strings representing the module
    names to exclude
  @param [opts.excludeTasks] - String or array of strings representing the tasks
    to exclude from the returned object
  @param [opts.transform] - Transformation function with prototype
    `function (taskName) -> newTaskName` for the imported task names

  @returns An object with shape `{taskName -> taskFunction}`; each function will
    be named according to its `displayName` property or, if that's is not
    defined, to its `name` property; if provided, all exported function names will
    be processed by the optional `transform` function.
*/
function readTasks(taskDir = DEFAULT_TASK_DIR, opts = {}) {
  /* eslint-disable no-param-reassign, prefer-rest-params */
  if (arguments.length === 1 && typeof arguments[0] === 'object') {
    [opts] = arguments;
    taskDir = DEFAULT_TASK_DIR;
  }
  /* eslint-enable no-param-reassign, prefer-rest-params */

  let { excludeTasks } = opts;
  const { exclude, transform } = opts;

  if (transform && typeof transform !== 'function') throw new Error('"transform" must be a function');
  if (excludeTasks) {
    if (!Array.isArray(excludeTasks)) {
      excludeTasks = [excludeTasks]; // eslint-disable-line no-param-reassign
    }
    if (excludeTasks.length && typeof excludeTasks[0] !== 'string') {
      throw new Error('"excludeTasks" must be an array of strings or a single string');
    }
  }

  const taskMods = readConfig(taskDir, { exclude });
  const transformFunc = transform || (x => x);

  return Object.fromEntries(
    [].concat(
      ...Object.values(taskMods).map(mod => (
        Object.values(mod)
          .filter(e => typeof e === 'function')
          .map((f) => {
            const fName = transformFunc(f.displayName || f.name);
            const taskF = (...args) => f(...args);

            taskF.displayName = fName;

            return [fName, taskF];
          })
      ))
    ).filter(
      f => !(excludeTasks || []).includes(f[0])
    )
  );
}

module.exports = {
  DEFAULT_TASK_DIR,
  DEFAULT_CONFIG_DIR,
  resolvePaths,
  readConfig,
  readTasks
};
