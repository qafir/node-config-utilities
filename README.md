# Configuration utilities

Collection of utilities for configuration management.

[![pipeline status](https://gitlab.com/qafir/node-config-utilities/badges/master/pipeline.svg)](https://gitlab.com/qafir/node-config-utilities/commits/master)
[![coverage report](https://gitlab.com/qafir/node-config-utilities/badges/master/coverage.svg)](https://gitlab.com/qafir/node-config-utilities/commits/master)

## Functions

### `resolvePaths(paths, [opts])`

Resolve paths in a configuration object. The `path` configuration object is expected to be configured as follows:

    paths = {
        myFolder1: '.',
        myFolder2: '/home',
        specialFile: '$myFolder2/special.txt'
    }

The output of `resolvePaths()` for this input would be:

    resolvePaths(paths) = {
        myFolder1: '/my/current/working/directory/full/path',
        myFolder2: '/home',
        specialFile: '/home/special.txt'
    }

The following rules apply:

* Dollar substitution only applies to the first entry of the path
* Behaviour for path names containing forward slashes is **undefined**
* Regardless of the operating system, input paths must be all forward-slash-separated

#### Options

* baseDir: Base folder path for relative entries resolution (*defaults to current directory*)
* iterationLimit: Iteration limit to avoid circular dependencies (*defaults to 1000; only needed for huge configuration objects*)
* normalize: Normalize returned slashes to forward-slashes (*default is false*)
* relative: True to have the output paths relative to the base directory (*default is false*)

### `readConfig([cfgDir = 'cfg'], [opts])`

Read configuration files from folder and return an object containing the exported configuration. Configuration files shall be in a *requireable* format as the returned data from `require()` will be the content of the configuration. As an example, a project with the following configuration:

```
  package.json
  cfg/
    paths.js
    browserify.js
    my-package.js
  gulpfile.js
```

Where *cfg* is the folder containing the configuration, could at some point call `readConfig()` to return an object with the following structure:

```javascript
  readConfig() = {
    // configKey: configContent
    //  configKey can be customised based on the module name and contents
    //  if the 'transform' option is specified
    paths: {...}, // Content from cfg/paths.js
    browserify: {...}, // Content from cfg/browserify.js
    'my-package': {...} // Content from cfg/my-package.js
  }
```

#### Options
* exclude: Array of module names (without the '.js' extension) to exclude from the import process; the module paths shall all be relative to the configuration folder
* transform: Transform function used to map module names to configuration keys; transform functions shall have prototype `function(modName, modContent) -> String`. The returned string will be used as the configuration key for the given module


### readTasks([taskDir = 'tasks'], [opts])

Read tasks (functions) from folder and return an object containing the exported tasks under their name (or display name if defiend). This function does not expose the module objects or anything contained in them which is not a function (even if exported).

Given the following folder structure:

```
  package.json
  tasks/
    compile.js
    clean.js
  gulpfile.js
```

Where:

```javascript
  // tasks/compile.js
  module.exports = {
    compile: function () {...}
  }

  // tasks/clean.js
  function cleanAll () {...}

  cleanAll.displayName = 'clean-all';

  module.exports = {
    clean: function () {...},
    cleanAll
  }
```

Calling `readTasks()` would result in:

```javascript
  readTasks() = {
    compile: function () {...},
    clean: function () {...},
    'clean-all': function () {...}
  }
```

#### Options
* exclude: Array of module names (without the '.js' extension) to exclude from the import process; the module paths shall all be relative to the tasks folder
* excludeTasks: String or array of strings representing the tasks to exclude from the returned object
* transform: Transform function used to map task names to configuration keys; transform functions shall have prototype `function(taskName, taskContent) -> String`. The returned string will be used as the configuration key for the given task

## License

This package and all of its contents are published under the MIT license. See the enclosed LICENSE file.
